//
//  Personajes.swift
//  EjemploSwiftUI
//
//  Created by Catalina on 08/10/19.
//  Copyright © 2019 Catalina. All rights reserved.
//

import SwiftUI

struct Personajes: View {
        var nombre : String = "Sergio"
        var apellido : String = "Rodriguez"
        var body: some View {
            VStack{
                HStack{
                    VStack{
                        HStack{
                            Text(nombre)
                                .font(.body)
                                .foregroundColor(.blue)
                            Text(apellido)
                            .font(.body)
                            .foregroundColor(.blue)
                        }
                    }
                }
            }
        }
}

struct Personajes_Previews: PreviewProvider {
    static var previews: some View {
        Personajes()
    }
}
