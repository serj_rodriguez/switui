//
//  ObservableModel.swift
//  EjemploSwiftUI
//
//  Created by Catalina on 08/10/19.
//  Copyright © 2019 Catalina. All rights reserved.
//

import Foundation
final class ObservableModel: ObservableObject{
    
    @Published var contador : Int = 0
    func incrementar(){
        self.contador += 1
    }
}
