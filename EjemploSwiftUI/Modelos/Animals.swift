//
//  Animals.swift
//  EjemploSwiftUI
//
//  Created by Catalina on 07/10/19.
//  Copyright © 2019 Catalina. All rights reserved.
//

import Foundation

let animales = [Animal(imagen: "dogtor",
                       nombreAnimal: "Dogtor",
                       nombreCientifico: "Perrogtor",
                       edadPromedio: "+100 años"),
                Animal(imagen: "gorila",
                       nombreAnimal: "Gorila",
                       nombreCientifico: "Gor",
                       edadPromedio: "-60 años"),
                Animal(imagen: "panda",
                       nombreAnimal: "Panda",
                       nombreCientifico: "Pan",
                       edadPromedio: "10 a 15 años")]

struct Animal: Hashable{
    var id = UUID()
    var imagen: String
    var nombreAnimal: String
    var nombreCientifico: String
    var edadPromedio: String
}
