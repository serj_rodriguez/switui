//
//  Personajes.swift
//  EjemploSwiftUI
//
//  Created by Catalina on 08/10/19.
//  Copyright © 2019 Catalina. All rights reserved.
//

import Foundation

var personajes = [Personaje(nombre: "Sergio", apellido: "Rodriguez")]
struct Personaje: Hashable{
    var id = UUID()
    var nombre: String
    var apellido: String
}
