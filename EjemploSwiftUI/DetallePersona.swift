//
//  DetallePersona.swift
//  EjemploSwiftUI
//
//  Created by Catalina on 08/10/19.
//  Copyright © 2019 Catalina. All rights reserved.
//

import SwiftUI

struct DetallePersona: View {
    var selectedPerson: Personaje
    var body: some View {
        VStack{
            HStack{
                Text("Nombre: ")
                Text(selectedPerson.nombre)
//                TextField("Ingresa tu nombre", text: selectedPerson.nombre)
            }
            HStack{
                Text("Apellido: ")
                Text(selectedPerson.apellido)
//                TextField("Ingresa tu apellido", text: selectedPerson.apellido)
            }
        }
    }
}

struct DetallePersona_Previews: PreviewProvider {
    static var previews: some View {
        DetallePersona(selectedPerson: Personaje(nombre: "Sergio", apellido: "Rodriguez"))
    }
}
