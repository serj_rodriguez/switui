//
//  DemoTabla.swift
//  EjemploSwiftUI
//
//  Created by Catalina on 07/10/19.
//  Copyright © 2019 Catalina. All rights reserved.
//

import SwiftUI

struct DemoTabla: View {
    var body: some View {
        NavigationView{
            List(animales, id: \.self){animal in
                NavigationLink(destination: ContentView(imagen: animal.imagen,
                nombreAnimal:animal.nombreAnimal,
                nombreCientifico: animal.nombreCientifico,
                edadPromedio: animal.edadPromedio)){
                    ContentView(imagen: animal.imagen,
                                nombreAnimal:animal.nombreAnimal,
                                nombreCientifico: animal.nombreCientifico,
                                edadPromedio: animal.edadPromedio)
                }
            }
            .navigationBarTitle(Text("Animales"))
        }
        
    }
}

struct DemoTabla_Previews: PreviewProvider {
    static var previews: some View {
        DemoTabla()
    }
}
