//
//  ContentView.swift
//  EjemploSwiftUI
//
//  Created by Catalina on 07/10/19.
//  Copyright © 2019 Catalina. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    var imagen : String = ""
    var nombreAnimal : String = ""
    var nombreCientifico : String = ""
    var edadPromedio : String = ""
    var body: some View {
        VStack{
            HStack{
                Image(imagen)
                    .resizable()
                    .frame(width: 100,
                           height: 100,
                           alignment: .center)
                    .clipShape(Circle())
                    .shadow(radius: 60)
                VStack{
                    HStack{
                        Text("Nombre:")
                            .font(.body)
                            .foregroundColor(.black)
                        Text(nombreAnimal)
                            .font(.body)
                            .foregroundColor(.blue)
                    }
                    HStack{
                        Text("Nombre Cientifico:")
                            .font(.body)
                            .foregroundColor(.black)
                        Text(nombreCientifico)
                            .font(.body)
                            .foregroundColor(.blue)
                    }
                    HStack{
                        Text("Promedio de vida:")
                            .font(.body)
                            .foregroundColor(.black)
                        Text(edadPromedio)
                            .font(.body)
                            .foregroundColor(.blue)
                    }
                }
            }
            
            //Preview para distintos devices
//            struct VistaAnimal_Previews: PreviewProvider {
//                static var previews: some View {
//                    Group {
//                    VistaAnimal(nombreAnimal: "Pato")
//                     /* .previewLayout(.fixed(width: 400.0, height: 100.0))*/
//                    .previewDevice(PreviewDevice(rawValue: "iPhone 11 Pro Max"))
//
//                    VistaAnimal(nombreAnimal: "Pato")
//                     /* .previewLayout(.fixed(width: 400.0, height: 100.0))*/
//                    .previewDevice(PreviewDevice(rawValue: "iPhone 11"))
//                    }
//                }
//            }
            ////////////////////////////
            
            //            HStack{
            //                Text("Nombre:")
            //                .font(.body)
            //                .foregroundColor(.blue)
            //                Text("Sergio")
            //                    .font(.body)
            //                    .foregroundColor(.blue)
            //            }
            //            HStack{
            //                Text("Apellido:")
            //                .font(.body)
            //                .foregroundColor(.blue)
            //                Text("Rodriguez")
            //                    .font(.body)
            //                    .foregroundColor(.blue)
            //            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
