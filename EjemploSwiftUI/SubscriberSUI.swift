//
//  SubscriberSUI.swift
//  EjemploSwiftUI
//
//  Created by Catalina on 08/10/19.
//  Copyright © 2019 Catalina. All rights reserved.
//

import SwiftUI

struct SubscriberSUI: View {
    @EnvironmentObject var observableModel: ObservableModel
    var body: some View {
        VStack{
            Text("Cuenta: ")
                .font(.headline)
                + Text("\(self.observableModel.contador)")
            Button(action: {
                self.observableModel.incrementar()
            }) {Text("Incrementar contador")}
        }
    }
}

struct SubscriberSUI_Previews: PreviewProvider {
    static var previews: some View {
        SubscriberSUI()
        .environmentObject(ObservableModel())
    }
}
