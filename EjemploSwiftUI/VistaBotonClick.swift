//
//  VistaBotonClick.swift
//  EjemploSwiftUI
//
//  Created by Catalina on 08/10/19.
//  Copyright © 2019 Catalina. All rights reserved.
//

import SwiftUI

struct VistaBotonClick: View {
    @State var personaje = personajes
    @State var nom = ""
    @State var ap = ""
    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    Text("Nombre: ")
                    TextField("Escribe tu nombre", text: self.$nom)
                        .multilineTextAlignment(.center)
                }
                HStack {
                    Text("Apellido: ")
                    TextField("Escribe tu apellido", text: self.$ap)
                        .multilineTextAlignment(.center)
                }
                Button(action: {
                    self.personaje.append(Personaje(nombre: self.nom, apellido: self.ap))
                    self.ap = ""
                    self.nom = ""
                }) {
                    Text("Agregar")
                }
                
                List(personaje, id: \.self){personaje in
                    NavigationLink(destination: DetallePersona(selectedPerson: personaje)) {
                        Personajes(nombre: personaje.nombre, apellido: personaje.apellido)
                    }
                }
            .navigationBarTitle(Text("Agregar personas"))
            }
        }
    }
}

struct VistaBotonClick_Previews: PreviewProvider {
    static var previews: some View {
        VistaBotonClick()
    }
}
